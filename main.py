import sys
import random
from direct.showbase.ShowBase import ShowBase
from panda3d.core import loadPrcFile

from panda3d.core import AmbientLight, Spotlight, DirectionalLight, AntialiasAttrib, WindowProperties, CollisionTraverser
from panda3d.physics import ForceNode, LinearVectorForce

#from panda3d.bullet import BulletWorld

from game.background import Background
from game.level import Level
from game.input import Input
from game.player import Player


class Game(ShowBase):

    def __init__(self):
        loadPrcFile('config.prc') 
        ShowBase.__init__(self)
        
        base.setBackgroundColor(.12,.12,.12)
        base.setFrameRateMeter(True)
        base.enableParticles()

        render.setShaderAuto()
        render.setAntialias(AntialiasAttrib.MAuto)

        base.cTrav = CollisionTraverser("SeeTravis")
        #base.cTrav.showCollisions(render)

        gravity_fn = ForceNode('gravity')
        gravity_fnp = render.attach_new_node(gravity_fn)
        gravity_force = LinearVectorForce(0,0,-9.81)
        gravity_fn.add_force(gravity_force)
        base.physicsMgr.addLinearForce(gravity_force)

        self.render = render

        self.input = Input(self)

        self.playing = False
        self.title = True

        self.l = Level(0)
        self.l.reparent_to(render)

        pos = self.l.get_start()
        self.p = Player(self, pos)

        self.make_ambient_light()
        self.make_light()

        self.camera.set_pos(20, 10, 10)
        #base.disableMouse()

        self.bg = Background(self.cam, render)

        self.accept("escape", sys.exit)

        taskMgr.add(self.update, 'GameUpdate')


    def make_ambient_light(self):
        alight = AmbientLight('ambient')
        alight.set_color((.3,.3,.3,.3))
        node = self.render.attach_new_node(alight)
        render.set_light(node)

    def make_light(self):
        light = DirectionalLight('light')
        light.set_color((.7,.7,.7,1))
        light.set_shadow_caster(True, 1024, 1024)
        light_np = self.render.attach_new_node(light)
        light_np.set_pos(15, 15, 15)
        light_np.look_at(0, 0, 0)
        self.render.setLight(light_np)

    def update(self, task):
        dt = globalClock.getDt()
        ft = globalClock.getFrameTime()

        #self.camera.look_at(self.p.np)
        self.p.update(dt)
        self.l.update(dt)
        return task.cont


if __name__ == '__main__':
    app = Game()
    app.run()