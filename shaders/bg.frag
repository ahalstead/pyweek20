#version 130

out vec4 frag_color;
in vec2 texCoord;
uniform float osg_FrameTime;

#define black vec4(0.0);
#pragma include "noise.glsl"

void main() {
  float frame = floor(osg_FrameTime);
  float frame_next = ceil(osg_FrameTime);


  vec2 seed = texCoord + vec2(sin(frame), cos(frame));
  vec2 seed_next = texCoord + vec2(sin(frame_next), cos(frame_next));

  bool show = snoise(seed) > .92;
  bool nextshow = snoise(seed_next) > .92;

  if(!show && !nextshow) discard;

  vec2 seed2 = texCoord - vec2(sin(frame), cos(frame));
  vec2 seed3 = texCoord + vec2(sin(frame), cos(frame) * 64);

  vec2 seed2_next = texCoord - vec2(sin(frame_next), cos(frame_next));
  vec2 seed3_next = texCoord + vec2(sin(frame_next), cos(frame_next) * 64);

  vec4 color_this = vec4(snoise(texCoord), snoise(seed2), snoise(seed3), 1.0);
  vec4 color_next = vec4(snoise(texCoord), snoise(seed2_next), snoise(seed3_next), 1.0);


  vec4 this_fc = show ? color_this : black;
  vec4 next_fc = nextshow ? color_next : black;

  frag_color = mix(this_fc, next_fc, osg_FrameTime - frame);
  //gl_FragColor = color2 * gradientValue + color1 * (1.0 - gradientValue);
  //frag_color = vec4(texCoord, 0, 1);
}


