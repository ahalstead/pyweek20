#version 130

in vec4 p3d_Vertex;
uniform mat4 p3d_ModelViewProjectionMatrix;
out vec2 texCoord;

void main()  {
  gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
  texCoord = p3d_Vertex.xz; 

  //vec3 dir = normalize(p3d_Vertex.xyz);
  //float angle = atan(dir.x, dir.y) / (2 * 3.14159265359);
  //texCoord = vec2(angle, dir.z);
  //texCoord = p3d_Vertex.xy;
}