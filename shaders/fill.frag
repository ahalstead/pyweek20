#version 130
#pragma include "noise.glsl"


in vec2 texCoord;
in float transparency;
out vec4 frag_color;
uniform float osg_FrameTime;

void main() {
	float frame = osg_FrameTime*80;
	float r = snoise(texCoord + vec2(frame, -frame));
	vec4 c;
	if (r > 0.4) {
		c = vec4(0.1);
	}
	else {
		c = vec4(0.9);
	}
	c.w = transparency;
	frag_color = c;
}