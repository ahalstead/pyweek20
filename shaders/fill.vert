#version 130

in vec4 p3d_Vertex;
uniform mat4 p3d_ModelViewProjectionMatrix;
out vec2 texCoord;

void main()  {
  gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
  texCoord = (vec2(gl_Position.x, gl_Position.y) + vec2(1.0)) / vec2(2.0);
}