framebuffer-multisample 1
multisamples 8
sync-video #f
window-title le game
icon-filename assets/game.ico
win-size 1280 720
model-path models

egg-object-type-wall <Collide> wall { Polyset keep descend } <Scalar> into-collide-mask { 0b0001 }
egg-object-type-start <Collide> start { Polyset descend } <Scalar> into-collide-mask { 0b0010 }
egg-object-type-end <Collide> end { Polyset descend intangible } <Scalar> into-collide-mask { 0b0011 }
egg-object-type-data <Collide> data { Polyset descend intangible } <Scalar> into-collide-mask { 0b0100 }
egg-object-type-spawn-grunt
