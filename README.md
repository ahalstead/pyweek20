game
====

With the panda SDK installed, `ppython game.py` on Windows, or `python game.py` on Linux, from the game directory.

Without the panda SDK installed, please try a binary. (?)

Credits
=======

Art, Code: Team Hopeless Opus

Music preformed live in one take by

Sam Sartorius - https://www.youtube.com/channel/UC5B7wKbvXOam4WWeKmw036w
extended S-wave - https://soundcloud.com/extended-s-wave

Special thanks:

rdb <https://github.com/rdb> and tobspr <https://github.com/tobspr> in #panda3d on irc.freenode.net - wouldn't have finished without their help and the awesome Panda3d engine!