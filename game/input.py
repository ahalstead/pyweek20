import math
try:
    import pyglet.input
    import pyglet.app
except:
    pyglet = None
    print("pyglet import failed - no joysticks for you")

AXIS_ENGAGE_THRESHOLD = 0.359

class Input(object):
    def __init__(self, base):
        self.state = {}
        self.keymap = {
            'left': ['a', 'arrow_left', 'shift-a', 'axis-x-neg'],
            'right': ['d', 'arrow_right', 'shift-d', 'axis-x-pos'],
            'forward': ['w', 'arrow_up', 'shift-w', 'axis-y-neg'],
            'backward': ['s', 'arrow_down', 'shift-s', 'axis-y-pos'],
            'trace': ['lshift', 'rcontrol', 'Button 0'],
            'boost': ['space', '0', 'ctrl-0', 'Button 1'],
            'target': ['f'] 
        }

        for action, buttons in self.keymap.iteritems():
            self.state[action] = 0
            for button in buttons:
                base.accept(button, self.setkey, [action, 1])
                base.accept(button+'-up', self.setkey, [action, 0])

        self.joystick = None
        if hasattr(pyglet, 'input'):
            pass
            self.init_joystick()
            pyglet.app.platform_event_loop.start()
            taskMgr.add(self._pyglet_loop, 'pyglet-event-loop')

        self.axes = {}

    def init_joystick(self):
        self.joysticks = pyglet.input.get_joysticks()
        for joystick in self.joysticks:
            if not self.joystick:
                self.joystick = joystick

            joystick.on_joybutton_press = self._joystick_press
            joystick.on_joybutton_release = self._joystick_release
            joystick.on_joyaxis_motion = self._joystick_axis
            joystick.open()

    def _joystick_press(self, joystick, button):
        raw_name = joystick.button_controls[button].raw_name
        if raw_name.startswith('BTN_'):
            raw_name = raw_name[4:]
        if len(raw_name) == 1:
            raw_name = 'gamepad-' + raw_name
        messenger.send('%s' % (raw_name))
        # Make this the active joystick
        self.joystick = joystick

    def _joystick_release(self, joystick, button):
        raw_name = joystick.button_controls[button].raw_name
        if raw_name.startswith('BTN_'):
            raw_name = raw_name[4:]
        if len(raw_name) == 1:
            raw_name = 'gamepad-' + raw_name
        messenger.send('%s-up' % (raw_name))

    def _joystick_axis(self, joystick, axis, value):
        if not axis in self.axes:
            self.axes[axis] = 0
        if abs(value) > AXIS_ENGAGE_THRESHOLD:
            val = math.ceil(value) if value > 0 else math.floor(value)
            if self.axes[axis] != val:
                self.axes[axis] = val 
                sign = "pos" if self.axes[axis] > 0 else "neg"
                messenger.send('axis-%s-%s'% (axis,sign))
        else:
            if not self.axes[axis] == 0:
                sign = "pos" if self.axes[axis] > 0 else "neg"
                self.axes[axis] = 0
                messenger.send('axis-%s-%s-up' % (axis,sign))

    def _pyglet_loop(self, task):
        pyglet.app.platform_event_loop.step(0.003)
        return task.cont

    def setkey(self, ev, arg):
        self.state[ev] = arg

    def get_state(self, action):
        return True if self.state[action] > 0 else False