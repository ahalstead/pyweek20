from panda3d.core import CollisionNode, CollisionSphere, CollisionRay, LRotationf, ColorAttrib, Point3, Shader
from panda3d.core import CollisionHandlerQueue, Vec3, LineSegs, NodePath, CollisionHandlerPusher, CollisionBox
from panda3d.core import TransparencyAttrib, PTAFloat
from direct.interval.LerpInterval import LerpColorScaleInterval, LerpFunc
from direct.interval.IntervalGlobal import Sequence, Func
from game.util.geom import GeomBuilder

class Player(object):
    def __init__(self, base, start_pos=(0,0,0)):
        gb = GeomBuilder('hero')
        color = (.2, .9, .2, 1)
        gb.add_dome(color, (0,0,0), 1, 4, 2, LRotationf(0,90,60))
        geom = gb.get_geom_node()
        geom.setAttrib(ColorAttrib.makeVertex())

        self.np = NodePath('heronp')
        self.model = NodePath(geom)
        self.model.reparent_to(self.np)
        self.np.reparent_to(base.render)
        self.np.set_pos(start_pos)
        self.model.set_scale(1,1,4)
        self.model.set_p(20)

        self.on_ground = True
        if(start_pos.z > .2):
            self.on_ground = False

        cs = CollisionSphere(0,0,0,1.2)
        self.csnp = self.np.attachNewNode(CollisionNode('body'))
        self.csnp.node().addSolid(cs)
        #self.csnp.set_scale(1,1,1)
        self.csnp.show()

        #ray = CollisionRay(0,0,0,0,0,-1)
        #self.raynp = self.model.attach_new_node()
        #self.raynp.node().add_solid(ray)
        #self.raynp.node().set_from_collide_mask(0)
        #self.raynp.set_pos(self.raynp, 0,0,.5)
        #self.raynp.show()

        #self.pusher = CollisionHandlerPusher()
        #self.pusher.addCollider(self.csnp, self.actornp)
        #base.cTrav.addCollider(self.csnp, self.pusher)

        self.wall_handler = CollisionHandlerPusher()
        self.wall_handler.addCollider(self.csnp, self.np)
        self.wall_handler.addInPattern('%(data)ihdata-entered')
        self.wall_handler.addInPattern('%(end)ihend-entered')
        self.wall_handler.addInPattern('%(trace)ihtrace-entered')
        base.cTrav.addCollider(self.csnp, self.wall_handler)

        base.accept('data-entered', self.get_data)
        base.accept('end-entered', self.do_end)
        base.accept('trace-entered', self.close_trace)


        self.input = base.input
        self.start_pos = start_pos
        self.pos = start_pos
        
        ls = LineSegs('dir')
        ls.setColor(1,1,1)
        ls.moveTo(0,0,0)
        self.dir = Vec3(0,-1,0)
        ls.drawTo(self.dir * 3)

        #lsnp = NodePath(ls.create())
        #lsnp.setColorOff()
        #lsnp.reparent_to(self.model)
        #self.dir_line = lsnp
        self.render = base.render
        end_pos = Vec3(*start_pos)
        end_pos.z = -1

        self.trace = False
        self.prev_points = []
        self.line_bits = []
        self.t_timer = 0

        self.data_collected = 0
        self.data_collected_names = set()

    def get_data(self, entry):
        into_node = entry.getIntoNodePath()
        name = into_node.getName()
        messenger.send('data-taken', [name])

        if name in self.data_collected_names:
            return
        else:
            self.data_collected_names.add(name)
            self.data_collected += 1


    def close_trace(self, entry):
        into_node = entry.getIntoNodePath()
        name = into_node.getName()
        if name == 'linenode-0':
            #skip the very first node we dropped
            return
        print entry, name

        #at least 3 points needed for a fill
        last_pt = name == 'linenode-'+str(len(self.line_bits))
        second_to_last_pt = name == 'linenode-'+str(len(self.line_bits)-1)

        if not last_pt and not second_to_last_pt:
            pt_hit = int(name.split('-')[1])
            node = self.render.attach_new_node(self.get_fill_geom(pt_hit))
            node.set_shader(Shader.load(Shader.SL_GLSL, 'shaders/fill.vert', 'shaders/fill.frag'))
            node.set_shader_input('transparency', 1.0)
            node.setTransparency(TransparencyAttrib.MAlpha)
            def _set_alpha(t):
                print t
                node.set_shader_input('transparency', t)
            def _removefill():
                node.removeNode()
                cintv = None
            updatealpha = LerpFunc(_set_alpha, fromData = 1.0, toData = 0.0, duration=5.0)
            removefill = Func(_removefill)
            Sequence(updatealpha, removefill).start()

        self.trace = False
        self.input.state['trace'] = 0
        self.stop_drawing()



    def get_fill_geom(self, pt_hit):
        color = (1,1,1,1)
        pts = [lb[0] for lb in self.line_bits[pt_hit:]]
        gb = GeomBuilder()
        for i in xrange(1, len(pts)):
            if i == 0:
                gb.add_tri(color, [pts[i], pts[-1], pts[-2]])
            if i > 0 and i < len(pts) - 1:
                gb.add_tri(color, [pts[0], pts[i], pts[i+1]])
        return gb.get_geom_node()

    def do_end(self, entry):
        print entry

    def update(self, dt):
        prev_pos = self.np.get_pos()
        #entries = list(self.wall_handler.getEntries())
        #print len(entries)
        #if len(entries) > 0:

            #print entries[0].getIntoNode().getName()
            #self.np.set_z(entries[0].getSurfacePoint(self.render).get_z())
            #print 'hit ground'

        if self.np.get_z() > .1 and not self.on_ground:
            self.np.set_z(self.np.get_z() - 6 * dt)
        else:
            self.on_ground = True
            self.np.set_z(.1)

        if self.input.get_state('forward'): 
            self.np.set_pos(self.np, 0, 20 * dt, 0)

        if self.input.get_state('backward'):
            self.np.set_pos(self.np, 0, -20 * dt, 0)

        if self.input.get_state('trace'):
            self.t_timer += dt
            if self.trace == False:
                self.prev_points.append(self.np.get_pos())
            self.trace = True
            if self.t_timer > .17:
                self.t_timer = 0
                self.add_line_seg()
            print len(self.prev_points)

        else:
            if self.trace:
                self.stop_drawing()
                self.trace = False

        angle = 0

        if self.input.get_state('left'):
            angle += 300 * dt

        if self.input.get_state('right'):
            angle -= 300 * dt

        self.np.set_h(self.np, angle)

    def add_line_seg(self):

        self.prev_points.append(self.np.get_pos())
        ls = LineSegs('tracelines')
        ls.set_thickness(5.0)
        ls.set_color(.9,.9,.1)
        if len(self.prev_points) <= 1:
            return
        start = self.prev_points[-2]
        end = self.np.get_pos()
        if start == end:
            self.prev_points.pop(-1)
            return
        ls.moveTo(start)
        ls.drawTo(end)

        line_bit = NodePath(ls.create())
        line_bit.reparent_to(self.render)
        
        pt = CollisionBox(Point3(start.x, start.y, start.z+1), .1, .1,2)
        pt.setTangible(False)
        csnp = line_bit.attach_new_node(CollisionNode('linenode-'+str(len(self.line_bits))))
        csnp.node().add_solid(pt)
        csnp.node().setIntoCollideMask(0xb0001)
        csnp.set_tag('trace', '1')
        csnp.show()
        
        self.line_bits.append((start, line_bit))

    def stop_drawing(self):
        self.prev_points = []
        for lb in self.line_bits:
            lb[1].removeNode()
        self.line_bits = []

    def get_dummy_node(self):
        pass
