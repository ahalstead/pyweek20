from game.util.geom import GeomBuilder
from panda3d.core import Shader, GeomNode, CullFaceAttrib, CompassEffect, LRotationf

class Background(object):
	def __init__(self, camera, render):
		geom = GeomNode('bg')
		gb = GeomBuilder('bg')
		gb.add_dome((1,1,1,1), (0,0,0), 150, 4, 3)
		gb.add_dome((1,1,1,1), (0,0,0), 150, 4, 3, LRotationf(0, 180, 90))
		geom.add_geom(gb.get_geom())
		
		self.node = render.attach_new_node(geom)
		self.node.set_depth_write(False)
		self.node.set_effect(CompassEffect.make(camera, CompassEffect.P_pos))
		self.node.set_attrib(CullFaceAttrib.make(CullFaceAttrib.MCullCounterClockwise))
		self.node.set_shader(Shader.load(Shader.SL_GLSL, 'shaders/bg.vert', 'shaders/bg.frag'))