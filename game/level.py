from panda3d.core import NodePath, DirectionalLight, AmbientLight
from direct.showbase import DirectObject
from game.playfield import Playfield
import random

levels = [
    {
        'name': 'tutorial',
        'ambient': (0.3, 0.6, 0.3, .2),
        'model': 'tutorial.egg',
        'data': 6,
        'required': 4,
        'spawn': [('grunt', 1)],
        'music': '1-tutorial.ogg'
    }
]

class Level(DirectObject.DirectObject):
    def __init__(self, number):
        self.name = levels[number]['name']
        self.model = loader.loadModel(levels[number]['model'])
        self.spawn = levels[number]['spawn']
        self.required = levels[number]['required']

        self.np = NodePath('level'+str(number))

        self.map = loader.loadModel("tutorial.egg")

        self.map.reparent_to(self.np)

        data = levels[number]['data']
        self.datas = {}        
        spawns = self.map.findAllMatches("data-*")
        for data_spawn in spawns:
            data_spawn.setTag('data', '1')
            self.datas[data_spawn.getName()] = self.make_data(data_spawn)

        self.make_2nd_light(levels[number]['ambient'])

        self.accept('data-taken', self.take_data)

        self.music = loader.loadSfx("music/"+levels[number]['music'])

    def reparent_to(self, other_np):
        self.np.reparent_to(other_np)

    def update(self, dt):
        for k,d in self.datas.iteritems():
            r1 = (300 * random.random()) * dt
            r2 = (300 * random.random()) * dt
            r3 = (300 * random.random()) * dt
            d.set_hpr(d, r1, r2, r3)

        if self.music.status() == self.music.READY:
            self.music.play()

    def make_2nd_light(self, color):
        light = AmbientLight('hilite')
        light.set_color(color)
        light_np = self.np.attach_new_node(light)
        self.np.setLight(light_np)

    def get_start(self):
        node = self.model.find('**/start')
        return node.get_pos()

    def make_data(self, data_spawn):
        model = loader.loadModel('misc/rgbCube')
        model.set_pos(data_spawn.get_pos())
        model.reparent_to(self.np)
        return model

    def take_data(self, dname):
        data = self.datas.pop(dname,None)
        if data != None:
            data.detach_node()

    def destroy(self):
        self.map.remove_node()
        self.ignoreAll()
