from game.util.geom import GeomBuilder
from panda3d.core import NodePath, LRotationf, ColorAttrib, Vec3, Point3, Plane, CollisionPlane, CollisionNode

class Playfield(object):
    def __init__(self, color=(.8, .8, .8, 1), size=(10,10)):
        gb = GeomBuilder('playfield')
        gb.add_block(color, (0,0,0), (size[0], size[1], .3))
        geom = gb.get_geom_node()
        geom.setAttrib(ColorAttrib.makeVertex())

        self.plane = CollisionPlane(Plane(Vec3(0,0,1), Point3(0,0,0)))


        self.np = NodePath('test')#geom)
        self.np.setColorOff()
        self.np.set_pos(self.np, 0,0,-.15)

        
        #self.np.setRenderModeWireframe()

    def find(self, search):
        return self.map.find(search)

    def reparent_to(self, np):
        self.np.reparent_to(np)
        self.map.reparent_to(np)

    def update(self):
        pass